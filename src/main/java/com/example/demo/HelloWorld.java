package com.example.demo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sherry on 17/2/20.
 */
@RestController
public class HelloWorld {

    /**
     * 日志
     */
    private Logger logger = LoggerFactory.getLogger(HelloWorld.class);

    @RequestMapping("/")
    public String helloworld(){
        return "Hello World 呵呵";
    }

}